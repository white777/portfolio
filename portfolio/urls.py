from django.conf.urls import patterns, include, url
from portfolio.views import *
from django.contrib import admin
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'portfolio.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', main, name="main"),
    url(r'^works/$', works, name="works"),
    url(r'^email/$', email_view),
    url(r'^works/(?P<id>\d+)/$', work, name="works"),
    url(r'^admin/', include(admin.site.urls)),
)
if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, }),
    )