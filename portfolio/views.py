__author__ = 'white'
from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.mail import send_mail, BadHeaderError
from Projects.models import *
from django.core.context_processors import csrf
from django.views.decorators.clickjacking import xframe_options_exempt


@xframe_options_exempt
def main(request):
    c = {}
    c.update(csrf(request))
    projects = Project.objects.all()
    c['projects'] = projects
    return render(request, 'main.html', c)

def works(request):
    c = {}
    projects = Project.objects.all()
    c['projects'] = projects
    return render(request, 'works.html', c)

def work(request, id):
    c = {}
    pages = ProjectPage.objects.filter(project=id)
    c['title'] = Project.objects.get(id=id)
    c['pages'] = pages
    return render(request, 'work.html', c)

def email_view(request):
    sender_name= request.POST.get('name', '')
    message = request.POST.get('message', '')
    from_email = request.POST.get('email', '')
    if sender_name and message and from_email:
            try:
                send_mail('From portfolio site', sender_name+'\r\n'+message, from_email, ['whitecomp777@yandex.ru'])
            except BadHeaderError:
                    return HttpResponse('Invalid header found.')
            return HttpResponseRedirect('Success!')
    else:
        all = ' '.join((sender_name, message, from_email))
        return HttpResponse('Fill the fields1\n'+all)
