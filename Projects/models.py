from django.db import models

from django.contrib.flatpages.models import *
# Create your models here.


class Project(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    image = models.ImageField(upload_to='.', blank=True)
    thumb = models.ImageField(upload_to='thumbs', editable=False)
    url = models.URLField(blank=True)
    date = models.DateField(auto_now=True)
    pr_types = (
        ('fsite', 'Site'),
        ('fverstka', 'Verstka'),
        ('fmodile', 'Mobile')
    )
    pr_type = models.CharField(max_length=30, choices=pr_types, default='Verstka')
    def get_url(self):
        if ProjectPage.objects.filter(project=self.id):
            return '/works/'+str(self.id)+'/'
        return self.url
    def get_image_url(self):
        a = ProjectPage.objects.filter(project=self.id)
        if a:
            return a[0].image.url
        return self.image.url
    def __unicode__(self):
        return self.title
    # pages = models.ManyToManyField('ProjectPage', related_name='+')
    def save(self, *args, **kwargs):
        if self.image:
            from PIL import Image
            from cStringIO import StringIO
            from django.core.files.uploadedfile import SimpleUploadedFile
            import os
            img = Image.open(self.image)

            if img.mode not in ('L', 'RGB'): img = img.convert('RGB')
            minSide = min(img.size)

            img = img.crop((0,0, minSide,
                      minSide))
            temp_handle = StringIO()
            img.save(temp_handle, 'png')
            temp_handle.seek(0)
            file_name, file_ext = os.path.splitext(self.image.name.rpartition('/')[-1])
            suf = SimpleUploadedFile(file_name + file_ext, temp_handle.read(), content_type='image/png')
            self.thumb.save(file_name + '.png', suf, save=False)
        super(Project, self).save(*args, **kwargs)


class ProjectPage(models.Model):
    project = models.ForeignKey(Project)
    url = models.URLField()
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='.')
    thumb = models.ImageField(upload_to='thumbs', editable=False)
    def save(self, *args, **kwargs):
        if self.image:
            from PIL import Image
            from cStringIO import StringIO
            from django.core.files.uploadedfile import SimpleUploadedFile
            import os
            img = Image.open(self.image)

            if img.mode not in ('L', 'RGB'): img = img.convert('RGB')
            minSide = min(img.size)

            img = img.crop((0,0, minSide,
                      minSide))
            temp_handle = StringIO()
            img.save(temp_handle, 'png')
            temp_handle.seek(0)
            file_name, file_ext = os.path.splitext(self.image.name.rpartition('/')[-1])
            suf = SimpleUploadedFile(file_name + file_ext, temp_handle.read(), content_type='image/png')
            self.thumb.save(file_name + '.png', suf, save=False)
        super(ProjectPage, self).save(*args, **kwargs)

