from django.contrib import admin
from Projects.models import *
# Register your models here.

class ProjectPageInline(admin.StackedInline):
    model = ProjectPage
class ProjectWithPages(admin.ModelAdmin):
    inlines = [ProjectPageInline]
admin.site.register(Project, ProjectWithPages)